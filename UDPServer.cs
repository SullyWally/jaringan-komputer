﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient udpclient = new UdpClient(8080);
            IPEndPoint ipep = null;

            string clientname = "";
            string servername = "";
            string smessage = "";
            string cmessage = "";

            byte[] rname = udpclient.Receive(ref ipep);
            clientname = Encoding.ASCII.GetString(rname);

            Console.WriteLine(clientname + " joined!");

            Console.Write("Input Username : ");
            servername = Console.ReadLine();

            byte[] sname = Encoding.ASCII.GetBytes(servername);
            udpclient.Send(sname, sname.Length, ipep);

            while (smessage != "bye")
            {
                byte[] rdata = udpclient.Receive(ref ipep);
                cmessage = Encoding.ASCII.GetString(rdata);

                Console.WriteLine(clientname + " : " + cmessage);

                Console.Write(servername + " : ");
                smessage = Console.ReadLine();

                byte[] sdata = Encoding.ASCII.GetBytes(smessage);
                udpclient.Send(sdata, sdata.Length, ipep);


            }

        }
    }
}